# Volume Backup and Restore
### Contianer의 Volume과 BindMount 백업 및 복구 


## Desc
- 특정 한 Container만 백업할 경우, Container명을 변수로 담아 스크립트를 수행한다. 
    + 현재 위치의 /backup 폴더 백업 datas 생성
- Container명 없이 스크립트를 실행할 경우, 전체 Container 백업
    + ex) Gitlab 컨테이너를 백업할 경우
        ```
        ./volume_backup.sh gitlab
        ./volume_restore.sh gitlab
        ```

- 백업 파일명 ex : {컨테이너명}_{볼륨명 또는 bindMount된 최하위 directory명}.tar
