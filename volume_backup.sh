CONTAINER=$1

[[ -z "$CONTAINER" ]] && CONTAINER_INFO=$(docker ps -qa --format "{{.ID}}:{{.Names}}") || CONTAINER_INFO=$(docker ps -qa --format "{{.ID}}:{{.Names}}" | grep $CONTAINER)
CONTAINER_IDS=$(echo "$CONTAINER_INFO" | awk '{ print $1 }')
CONTAINER_NAMES=$(echo "$CONTAINER_INFO" | awk '{ print $2 }')

echo $CONTAINER_INFO

## ContainerList
for CON in $CONTAINER_INFO  
do  
    CONTAINER_ID=$(echo $CON | cut -d: -f1)
    CONTAINER_NAME=$(echo $CON | cut -d: -f2)
    DATAS=$(docker inspect --format='{{json .Mounts}}' $CONTAINER_ID | jq -r '.[] | if .Type=="volume" then .Name + ":" + .Destination else (.Source | split("/") | last) + ":" + .Destination end')
    echo $DATAS

    ## Volume or Bind List
    for DATA in $DATAS; do
        DATA_NAME=$(echo $DATA | cut -d: -f1)
        DATA_PATH=$(echo $DATA | cut -d: -f2)
        ## Compress the Volume and BindMount files
        docker run --rm --volumes-from $CONTAINER_ID -v $(pwd)/backup:/backup ubuntu tar cvf /backup/$CONTAINER_NAME-$DATA_NAME.tar $DATA_PATH
    done
done
