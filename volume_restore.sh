CONTAINER=$1

regex="^(.*)${CONTAINER}-.*\.tar$"

#Container List
[[ -z "$CONTAINER" ]] && CONTAINER_LIST=$(docker ps -qa --format "{{.ID}}:{{.Names}}") || CONTAINER_LIST=$(docker ps -qa --format "{{.ID}}:{{.Names}}" | grep $CONTAINER)
CONTAINER_IDS=$(echo "$CONTAINER_INFO" | awk '{ print $1 }')
RESTORE_LIST=`ls | grep -oE $regex`
TEMP_CONTAINER=''
for RESTORE in $RESTORE_LIST  
do 
    ## Volume or Bind List
    # Filename = {CONTAINER_NAME}-{VOLUME_NAME}.tar
    RESTORE_CONTAINER_NAME=$(echo "${RESTORE}" | cut -d'-' -f1)
    VOLUME_NAME=$(echo "${RESTORE}" | sed -E "s/^([^_]+)-(.+)\.tar$/\2/")
    # Stop container
    echo ">>>>>>>>>>>>> [STOP] CONTAINER >>:"$RESTORE_CONTAINER_NAME
    docker stop $RESTORE_CONTAINER_NAME
    # restore
    docker run --rm --volumes-from $RESTORE_CONTAINER_NAME -v $(pwd):/backup ubuntu tar xvf /backup/$RESTORE
    # Start container
    docker start $RESTORE_CONTAINER_NAME
    echo ">>>>>>>>>>>>> [START] CONTAINER >>:"$RESTORE_CONTAINER_NAME
done
